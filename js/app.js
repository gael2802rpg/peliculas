const llamandoFetch = () => {

    const pelicula = document.getElementById('nombre').value;
    

    if(pelicula == ''){
        alert('Porfavor ingrese una pelicula antes de buscar')
    }
    else{
        const url = "https://www.omdbapi.com/?t="+pelicula+"=&apikey=30063268";
        fetch(url)
        .then((response) => response.json())
        .then((data) => mostrarDatos(data))
        .catch((error) => {
            const lblError = document.getElementById("lnlError");
            lblError.innerHTML = "Ocurrió un error: " + error;
        });
    }
    
};

const mostrarDatos = (data) => {
    let title = document.getElementById('getNombre');
    let year = document.getElementById('getYear');
    let img = document.getElementById('imagen');
    let res = document.getElementById('text');
    let act = document.getElementById('actores');
    
    title.innerHTML = data.Title;
    year.innerHTML = data.Year;
    img.src = data.Poster;
    res.innerHTML = data.Plot;
    act.innerHTML = data.Actors;

    //console.log(data);
};



document.getElementById("btnBuscar").addEventListener("click", llamandoFetch);



